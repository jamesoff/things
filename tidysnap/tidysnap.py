#!/usr/bin/env python

# Todo:
#  read archive sizes before and after, report savings
#  hide delete output from tarsnap
#  draw a calendar of when backups are available
#  command line options for age limits, which day in the week to keep etc

import sys
import subprocess
import re
import datetime
import calendar

from typing import List


def get_stats() -> List[int]:
    print("--> Getting stats")
    output = subprocess.Popen(
        ["tarsnap", "--print-stats"], stdout=subprocess.PIPE
    ).communicate()[0].decode("utf-8")
    _values = re.findall(r"(\d+)", output)
    values = [int(x) for x in _values]
    return values


def delete_archive(name: str) -> None:
    subprocess.Popen(
        ["tarsnap", "-d", "-f", name, "--no-print-stats"], stdout=subprocess.PIPE
    ).communicate()[0]


def tidy_archives() -> None:
    limit1 = datetime.timedelta(7)
    limit2 = datetime.timedelta(31)
    limit3 = datetime.timedelta(365)
    r = re.compile(
        r"^(?P<filename>\S+)\s+(?P<year>[0-9]{4})-(?P<month>[0-9]{2})-(?P<day>[0-9]{2})"
    )
    delete_list = []

    total = 0

    print("==> Fetching archive list...")
    archive_list = (
        subprocess.Popen(["tarsnap", "--list-archives", "-v"], stdout=subprocess.PIPE)
        .communicate()[0]
        .decode("utf-8")
    )

    now = datetime.date.today()
    c = calendar.Calendar()

    for line in sorted(archive_list.split("\n")):
        matches = r.match(line)
        if matches:
            total += 1
            year = int(matches.group("year"))
            month = int(matches.group("month"))
            day = int(matches.group("day"))
            filename = matches.group("filename")

            when = datetime.date(year, month, day)

            if (now - limit1) < when:
                print("    %s: keeping (newer than a week)" % filename)
                continue
            if (now - limit2) < when:
                # delete if not friday
                if when.weekday() == 4:
                    print(
                        "    %s: keeping (newer than a month; end of week)" % filename
                    )
                    continue
                # delete
                print("--> Delete %s (newer than a month; not end of week)" % filename)
                delete_list.append(filename)
                continue

            # delete if older than a year
            if (now - limit3) > when:
                print("--> Delete %s (older than a year)" % filename)
                delete_list.append(filename)
                continue
            # delete if not last day of month
            month_info = c.monthdatescalendar(year, month)
            last_week = month_info[len(month_info) - 1]
            for weekday in last_week:
                if weekday.month == month:
                    last_day = weekday
            if last_day == when:
                print("    %s: keeping (older than a month; month end)" % filename)
                continue
            print("--> Delete %s (older than a month)" % filename)
            delete_list.append(filename)
    print()
    sys.stdout.write("==> Deleting %d archives" % len(delete_list))
    sys.stdout.flush()
    count = 0
    for archive in delete_list:
        count += 1
        delete_archive(archive)
        if count % 10 == 0:
            sys.stdout.write("%d" % count)
            continue
        if count % 2 == 0:
            sys.stdout.write(".")

    print()
    print("==> Checked %d archives, deleted %d." % (total, len(delete_list)))


if __name__ == "__main__":
    before_stats = get_stats()
    tidy_archives()
    after_stats = get_stats()

    print("[before -> after (saving)]")
    print(
        "All archives   total: %15d -> %15d (%15d)    comp: %15d -> %15d (%15d)"
        % (
            before_stats[0],
            after_stats[0],
            (before_stats[0] - after_stats[0]),
            before_stats[1],
            after_stats[1],
            (before_stats[1] - after_stats[1]),
        )
    )
    print(
        "  (unique data)       %15d -> %15d (%15d)          %15d -> %15d (%15d)"
        % (
            before_stats[2],
            after_stats[2],
            (before_stats[2] - after_stats[2]),
            before_stats[3],
            after_stats[3],
            (before_stats[3] - after_stats[3]),
        )
    )
    print()
