#!/usr/bin/python

"""
Uses reflect.php to figure out external IP and then requests Amazon updates the given DNS record with that

Will look for configuration in /usr/local/etc/autodns.json, or you can pass the path as the first parameter.

Config is JSON and looks like this:

{
    'hosted_zone_id': 'Route 53 hosted zone ID',
    'dns_name': 'the.record.to.update',
    'topic_arn': 'SNS topic ARN to notify when there are problems',
    'alert_lock': 'A file to write to show an alert has already been sent',
    'reflect_url': 'https://your.server.com/reflect.php?apikey=YOUR_API_KEY'
}

jamesoff.net
"""

import sys
import os.path

import requests
import boto
import boto.sns
import socket
import json


def check_lock():
    try:
        if os.path.exists(CONFIG['alert_lock']):
            return True
    except:
        pass

    # it doesn't exist (probably) so create it
    try:
        fh = open(CONFIG['alert_lock'], "w")
        fh.close()
    except:
        pass

    return False


def clear_lock():
    try:
        os.unlink(CONFIG['alert_lock'])
    except:
        pass


def send_alert(message):
    if check_lock:
        print "Alert lock exists, not sending alert again."
        return
    try:
        sns = boto.sns.connect_to_region("eu-west-1")
        sns.publish(CONFIG['topic_arn'], message, "Alert from autodns")
    except Exception, e:
        print "Error sending SNS alert"
        print e
        sys.exit(1)

try:
    if len(sys.argv) == 2:
        config_file = sys.argv[1]
    else:
        config_file = '/usr/local/etc/autodns.json'

    with open(config_file, 'r') as fh:
        CONFIG = json.load(fh)

except Exception, e:
    print "Unable to load config file %s" % config_file
    print e
    sys.exit(1)

try:
    r = requests.get(CONFIG['reflect_url'])
except Exception, e:
    print "Error loading data from reflect service"
    print e
    send_alert("Error loading data from reflect service: %s", e)
    sys.exit(1)

if r.status_code != 200:
    print "Did not get status code 200 from remote service"
    send_alert("Did not get status 200 from reflect service")
    sys.exit(1)

try:
    if r.json()["error"]:
        print "Got error from remote service"
        sys.exit(1)
except:
    print "Got response 200 but had error parsing JSON"
    sys.exit(1)

try:
    current_dns = socket.gethostbyname(CONFIG['dns_name'])
    if current_dns == r.json()["your_ip"]:
        clear_lock()
        sys.exit(0)
except socket.gaierror, e:
    if e.errno == 8:
        print "No record currently exists for %s" % CONFIG['dns_name']
except Exception, e:
    print "Error looking up current IP"
    print e
    sys.exit(1)

try:
    route53 = boto.connect_route53()
    records = route53.get_all_rrsets(CONFIG['hosted_zone_id'])
    change1 = records.add_change("DELETE", CONFIG['dns_name'], "A")
    change1.add_value(current_dns)
    change2 = records.add_change("CREATE", CONFIG['dns_name'], "A")
    change2.add_value(r.json()["your_ip"])
    records.commit()
except Exception, e:
    print "Error changing record at Amazon"
    print e
    send_alert("Error changing record at Amazon; current IP is %s" % r.json()["your_ip"])
    sys.exit(2)

clear_lock()
