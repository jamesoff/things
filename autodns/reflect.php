<?php
	$api_key = "SOME_API_KEY";

	header("Content-type: application/json");

	if (isset($_GET['apikey']) && $_GET['apikey'] == $api_key) {
		$remote_ip = $_SERVER['REMOTE_ADDR'];
		if ($remote_ip == "") {
			$error = 'No remote IP';
		}
		else {
			$error = '';
		}

		if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$forwarded = 1;
			$forwarded_for = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else {
			$forwarded = 0;
			$forwarded_for = '';
		}

		$result = array(
			'your_ip' => $remote_ip,
			'error' => $error,

			'forwarded' => $forwarded,
			'forwarded_for' => $forwarded_for
		);

		print json_encode($result);
	}
	else {
		print json_encode(array(
			'error' => 'Bad API key'
		));
	}
?>
